from flask_restful.reqparse import Namespace
from db import db

from utils import _assign_if_something

class TypeModel(db.Model):
    __tablename__ = 'type'

    id = db.Column(db.BigInteger, primary_key = True)
    name = db.Column(db.String)
    description = db.Column(db.String)
    creation_date = db.Column(db.Date)

    def __init__(self, id, name, description, creation_date):
        self.id = id
        self.description = description
        self.name = name
        self.creation_date = creation_date
    
    def json(self, jsondepth = 0):
        json = {
            'id': self.id,
            'description': self.description,
            'name': self.name,
            'creation_date': self.creation_date
        }

        return json

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id = id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def from_reqparse(self, newdata: Namespace):
        for no_pk_key in ['description','name','creation_date']:
            _assign_if_something(self, newdata, no_pk_key)
